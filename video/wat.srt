﻿1
00:00:01,500 --> 00:00:03,588
Porozmawiajmy o JavaScript

2
00:00:07,594 --> 00:00:10,294
Czy ktokolwiek wie czym w JS jest

3
00:00:10,394 --> 00:00:12,894
tablica plus tablica?

4
00:00:24,000 --> 00:00:26,607
Pusty string

5
00:00:29,929 --> 00:00:33,429
Myślę, że to jest dla wszystkich oczywiste

6
00:00:33,400 --> 00:00:34,988
Teraz, czym jest tablica plus obiekt?

7
00:00:35,088 --> 00:00:36,588
To powinień być błąd typowania

8
00:00:36,688 --> 00:00:38,188
ponieważ to zupełenie różne typy

9
00:00:44,729 --> 00:00:46,729
To jest obiekt.

10
00:00:48,559 --> 00:00:52,059
Ponieważ to jest plus to możemy zamienić argumenty kolejnością

11
00:00:52,159 --> 00:00:54,159
i powinniśmy otrzymać ten sam wynik.

12
00:00:57,781 --> 00:01:00,181
Więc obiekt plus tablica powinien zwrócić nam ten sam wynik

13
00:01:01,000 --> 00:01:03,500
Co jak widać się dzieje :P

14
00:01:06,635 --> 00:01:11,335
A w końcu jedyny który jest prawdziwy jest

15
00:01:14,598 --> 00:01:18,500
obiekt plus obiekt daje nam NaN (nie liczba)

16
00:01:19,431 --> 00:01:21,031
więc, ten jest w zasadzie poprawny

17
00:01:40,207 --> 00:01:41,707
Porozmawiajmy o JavaScript

18
00:01:44,282 --> 00:01:46,282
Jeśli napiszę Array(16)

19
00:01:46,582 --> 00:01:51,582
otrzymam 16 przecinków [przyp. błąd - tam jest 15 przecinków]

20
00:01:51,682 --> 00:01:53,182
Co jest oczywiste

21
00:01:53,282 --> 00:01:56,782
i jeśli teraz złącze je ze stringiem

22
00:01:56,882 --> 00:01:59,182
otrzymam ten napis powielony 16 razy.

23
00:02:04,298 --> 00:02:06,298
Teraz jeśli wezmę ten napis i dodam do niego 1

24
00:02:06,398 --> 00:02:11,898
1 zostanie zrzutowane na stringa i otrzymujemy "wat1" co jest ok

25
00:02:11,998 --> 00:02:15,498
Ale czy ktoś wie co się stanie kiedy odejmiemy 1?

26
00:02:18,379 --> 00:02:20,879
Rozumiem, że nikt nie wie, zatem dam wam wskazówkę.

27
00:02:20,979 --> 00:02:22,979
Czy to pomogło?

